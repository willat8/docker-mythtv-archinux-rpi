FROM scratch

ADD ArchLinuxARM-2016.08-rpi-rootfs.tar.gz /

COPY qemu-arm-static /usr/bin/qemu-arm-static

# Install the old dependencies
RUN echo 'Server = http://tardis.tiny-vps.com/aarm/repos/2016/08/31/$arch/$repo' > /etc/pacman.d/mirrorlist \
 && pacman -Sy mythtv --noconfirm

# Install the new mythtv
RUN echo 'Server = http://au.mirror.archlinuxarm.org/$arch/$repo' > /etc/pacman.d/mirrorlist \
 && pacman -Syy mythtv libvpx nfs-utils --noconfirm

RUN ln -sfv /usr/share/zoneinfo/Australia/Sydney /etc/localtime \
 && sed -ri 's/^#(en_AU\.UTF-8 .*)/\1/' /etc/locale.gen \
 && locale-gen

ENV LC_ALL=en_AU.UTF-8 \
    LANG=en_AU.UTF-8

CMD mythbackend

